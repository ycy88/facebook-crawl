// tutorial see https://www.youtube.com/watch?v=1VBgdKkuyNs
// and here http://lorenstewart.me/2017/03/12/using-node-js-to-interact-with-facebooks-graph-api/

const rp = require('request-promise');
const express = require('express');

var app = express();

var port = 3000;



// routes
// first one is searchme - test searching my own fb acct 
require('./searchme.js')(app, rp);
require('./searchplaces.js')(app, rp);


app.listen(port, function(){
    console.log("Server now running on localhost: %d", port);
});



