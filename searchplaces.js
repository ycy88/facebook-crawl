var fs = require('fs');
var filename = './results.json';

// change token when expire
// get token from here: https://developers.facebook.com/tools/explorer
var aToken = "EAACEdEose0cBAL4WS2hghTo54L7MApAZAbmkGmHVZARFZCj2WD9gTJTZAyMP2VV7JFsL6Ksz6tywW43YLLZCTml0486kXekXX4qFJ6E26W72kj7isxp8R7szlrbeB1H28QKM1Y8soLoff4mc9X25zMQCSPDep09EL37E8ZBNq8nYgEyhZBoZBRltUIsdQrAaCqEZD";

module.exports = function (app, rp) {
    app.get(`/v2.9/search`, function(req, res) {
        var searchTerm = req.query.name || "diving";

        rp({
            method: 'GET',
            uri: `https://graph.facebook.com/v2.9/search`,
            qs: {
                access_token: aToken,
                q: searchTerm,
                type: 'place',
                categories: ["TRAVEL_TRANSPORTATION"],
                fields: 'name, location'
            }
        }
        )
        // fb returns a JSON STRING - use JSON.parse to convert back to JSON
        .then(function(result){
            console.log(`Query successful to https://graph.facebook.com/v2.9/search`);
            
            var fbData = JSON.parse(result).data; // returns JSON.object
            var dataArray = JSON.parse(fs.readFileSync(filename));
            for (var i in fbData) {
                dataArray.push(fbData[i]);
            }
            fs.writeFileSync(filename, JSON.stringify(dataArray, null, 4));
            res.send(`Written ${fbData.length} records to the file`);

            // if (result.paging.next) {
            //     // go to next page

            // }


        })
        .catch(function(err) {
            console.log(err);
        });
    });
};

// reading a JSON file change the contents into a JavaScript object/ array